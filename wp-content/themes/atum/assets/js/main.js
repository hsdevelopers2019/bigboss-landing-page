/* ------------------------------------- */
/*  TABLE OF CONTENTS
 /* ------------------------------------- */
/*   PRE LOADING                          */
/*   WOW                                 */
/*   Menu                                */
/*  STICKY HEADER                        */
/*   COUNTER                             */
/*   portfolio-filter                    */
/*   pop up                              */
/*   OWL CAROUSEL                        */
/*    MAPS                               */
/*  TEXT ANIMATE                         */
/*   TEXT ROTATOR                        *



/*--------------------------------------------*/
/*  PRE LOADING
 /*------------------------------------------*/
'use strict';
jQuery(window).on('load',function () {
    jQuery('.loader').delay(500).fadeOut('slow');
});

jQuery(document).ready(function() {

    'use strict';
    /* ------------------------------------- */
    /*   wow
     /* ------------------------------------- */
    var wow = new WOW(
        {
            animateClass: 'animated',
            offset: 10,
            mobile: true
        }
    );
    wow.init();
    
    /* ==============================================
     STICKY HEADER
     =============================================== */

    jQuery(window).on('scroll', function () {
        if (jQuery(window).scrollTop() < 100) {
            jQuery('.header').removeClass('sticky_header');
        } else {
            jQuery('.header').addClass('sticky_header');
        }
    });


    /* ==============================================
     Blog Masonry
     =============================================== */
    jQuery('.blog-wrapper').masonry({
        itemSelector: '.grid-item',
        columnWidth: '.grid-sizer',
        percentPosition: true,
        transitionDuration: 0
    });

    /* ==============================================
     Smooth Scroll To Anchor
     =============================================== */
    jQuery('a.has_sub_menu').on('click', function(e){
        if (window.matchMedia('(max-width: 992px)').matches){
            e.preventDefault();
            jQuery(this).toggleClass("active_menu");
            jQuery(this).next(jQuery('.sub_menu')).slideToggle();
        }
    });
    jQuery('a.has_sub_menuu').on('click', function(e){
        if (window.matchMedia('(max-width: 992px)').matches){
            e.preventDefault();
            jQuery(this).toggleClass("active_menu");
            jQuery(this).next(jQuery('.sub_menu')).slideToggle();
        }
    });
    jQuery('a.nav-link[href^="#"]').on('click', function (event) {
        var jQueryanchor = jQuery(this);
        jQuery('html, body').stop().animate({
            scrollTop: jQuery(jQueryanchor.attr('href')).offset().top
        }, 1500, 'easeInOutExpo');
        event.preventDefault();
    });

    /* ==============================================
     STICKY HEADER
     =============================================== */

    jQuery('.header_menu_btn , .overlay').click(function(){
        jQuery('.header_menu_btn').toggleClass('clicked');
        jQuery('.overlay').toggleClass('show');
        jQuery('.side_nav').toggleClass('show');
        jQuery('body').toggleClass('overflow');
    });

    jQuery(window).on('scroll', function () {
        if (jQuery(window).scrollTop() < 100) {
            jQuery('.header').removeClass('sticky_header');
            jQuery('#back-to-top').removeClass('active');
        } else {
            jQuery('.header').addClass('sticky_header');
            jQuery('#back-to-top').addClass('active');
        }
    });

    jQuery(window).on('scroll', function () {
        if (jQuery(window).scrollTop() < 400) {
            jQuery('#back-to-top').removeClass('active');
        } else {
            jQuery('#back-to-top').addClass('active');
        }
    });



    /* --------------------------------------------------------
     COUNTER JS
     ----------------------------------------------------------- */

    jQuery('.counter').counterUp({
        delay: 5,
        time: 3000
    });

    /* ==============================================
        portfolio-filter
        =============================================== */

    // filter items on button click
    jQuery('#filtr-container').on('click', 'li', function(e) {
        e.preventDefault();
        jQuery('#filtr-container li').removeClass('active');
        jQuery(this).closest('li').addClass('active');
    });

    jQuery(window).on('load',function(){
        var filtrContainer = jQuery('.filtr-container');
        if(filtrContainer.length > 0) {
            filtrContainer.filterizr();
        }
    });


    /* ==============================================
     pop up
     =============================================== */

    jQuery('.filtr-container').magnificPopup({
        delegate: 'a',
        type: 'image',
        tLoading: 'Loading image #%curr%...',
        mainClass: 'mfp-img-mobile',
        gallery: {
            enabled: true,
            navigateByImgClick: true,
            preload: [0,1] // Will preload 0 - before current, and 1 after the current image
        },
        image: {
            tError: '<a href="%url%">The image #%curr%</a> could not be loaded.',
            titleSrc: function(item) {
                return item.el.attr('title');
            }
        },
        zoom: {
            enabled: true,
            duration: 300, // don't foget to change the duration also in CSS
            opener: function (element) {
                return element.find('img');
            }
        }
    });

    jQuery('.popup_video').magnificPopup({
        disableOn: 700,
        type: 'iframe',
        mainClass: 'mfp-fade',
        removalDelay: 160,
        preloader: false,
        fixedContentPos: false
    });

    // /* ==============================================
    //  OWL CAROUSEL
    //  =============================================== */
    jQuery(".hero_carousel").owlCarousel({
        loop:true,
        autoplay:true,
        smartSpeed:550,
        autoplayHoverPause:false,
        dots:true,
        nav:true,
        navText:['<i class="lnr-arrow-left lnr"></i>','<i class="lnr-arrow-right lnr"></i>'],
        responsiveClass:true,
        items:1,
        animateOut: 'fadeOut',
        animateIn: 'fadeIn'
    });
    jQuery(".testimonial_carousel").owlCarousel({
        loop:true,
        autoplay:true,
        smartSpeed:550,
        autoplayHoverPause:false,
        dots:false,
        nav:false,
        items:1
    });

    jQuery(".blog_carousel").owlCarousel({
        loop:true,
        autoplay:true,
        smartSpeed:450,
        autoplayHoverPause:false,
        dots:false,
        nav:false,
        responsiveClass:true,
        responsive:{
            0:{
                items:1
            },
            450:{
                items:1

            },
            800:{
                items:2

            },
            1200:{
                items:3

            }
        },
        items:3
    });
    jQuery(".brand_carousel").owlCarousel({
        loop:true,
        autoplay:true,
        smartSpeed:450,
        autoplayHoverPause:false,
        dots:false,
        nav:false,
        responsiveClass:true,
        responsive:{
            0:{
                items:2
            },
            600:{
                items:3

            },
            1000:{
                items:5

            }
        },
        items:5
    });
        if(jQuery("#typed").length > 0){
            let typed = new Typed('#typed', {
                stringsElement: '#typed-strings',
                typeSpeed: 100,
                loop: true
            });
        }

        if(jQuery("#home_intro_video").length > 0){
         let video_id = jQuery("#home_intro_video").attr('data_video_id');
            jQuery('#home_intro_video').YTPlayer({
                fitToBackground: true,
                videoId: video_id
            });
        }
});